package com.softserveinc.shoppingconsumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("shopping-service")
public interface RestaurantFeignClient {

    @GetMapping("/restaurants/count")
    Integer getRestaurantsCount();
}
