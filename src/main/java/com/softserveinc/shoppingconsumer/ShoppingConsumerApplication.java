package com.softserveinc.shoppingconsumer;

import com.softserveinc.shoppingconsumer.client.RestaurantFeignClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableFeignClients
public class ShoppingConsumerApplication implements CommandLineRunner {

    private RestaurantFeignClient restaurantFeignClient;

    @Autowired
    public ShoppingConsumerApplication(RestaurantFeignClient restaurantFeignClient) {
        this.restaurantFeignClient = restaurantFeignClient;
    }

    public static void main(String[] args) {
        SpringApplication.run(ShoppingConsumerApplication.class, args);
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Load Balanced RestTemplate: Number of restaurants = " + restTemplate().getForEntity("http://shopping-service/restaurants/count", Integer.class).getBody());
        System.out.println("Load Balanced Feign Client: Number of restaurants = " + restaurantFeignClient.getRestaurantsCount());
    }
}
